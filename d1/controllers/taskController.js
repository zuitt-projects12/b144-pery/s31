// Controllers contain the functions and business logic of our Express JS app. Meaning all the operations can do will be placed in this file.
const Task = require("../models/task");

// Controller function for getting all the tasks.
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	});
};

// Controller function for creating a task
/*
	Business logic
	1. Add a functionality to check if there are duplicate tasks.
		- If the task already exist, return "there is a duplicate".
		- If the task doesn't exist, we can add it in the database.
	2. The task data will be coming from the request body.
	3. Create a new Task object with properties that we need.
	4. Then save the data.
*/
module.exports.createTask = (requestBody) => {
	return Task.findOne({name: requestBody.name}).then((result, err) => {
		if(err){
			console.log(err);
			return false;
		}
		else{
			if(result !== null && result.name === requestBody.name){
				return `${requestBody.name} exists!`;
			}
			else{
				// Create a task object based on the Mongoose model "Task"
				let newTask = new Task({
					name: requestBody.name
				});
				// Save the data.
				// The "then" method will accept the 2 parameters:
				// firstParameter - result returned by the mongoose "save" method
				// secondParameter - will store the "error" object
				return newTask.save().then((task, error) => {
					if(error){
						console.log(error);
						// If an error is encountered, the "return" statement will prevent any other line or code below and within the same code block from executing.
						// Since the following return statement is nested within the "then" method chained to the "save" method, they do not prevent each other from executing code.
						// The else statement will no longer be evaluated.
						return false;
					}
					else{
						// If save is successful, return the new task.
						return task;
					}
				});
			}
		}
	});
};

// Controller function for deleting a task.
/*
	Business Logic
	1. Look for the task with the corresponding id provided in the URL/route.
	2. Delete the task.
*/

module.exports.deleteTask = (taskId) => {
	// The findByIdAndRemove mongoose method will look for for a task with the same id provided from the URL and remove/delete the document from MongoDB. It looks for the document using the "_id" field
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err){
			console.log(err);
			return false;
		}
		else{
			return `${removedTask.name} has been deleted.`;
		}
	});
};

// Controller function for updating a task.
/*
	Business logic
	1. Find the task with the ID.
	2. Find any task with the same name.
		- If there is a duplicate, return "there is a duplicate".
		- If no duplicates, replace the task's name returned from the database with the "name" property from the request body.
	3. Save the task.
*/

module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, err) => {
		// If an error is encountered, return a false
		if(err){
			console.log(err);
			return false;
		}
		// Results of the "findById" method will be stored in the "result" parameter
		// Its "name" property will be reassigned the value of the name received from the content
		else{
			return Task.findOne({name: newContent.name}).then((result, err) => {
				if(err){
					console.log(err);
					return false;
				}
				else{
					if(result !== null && result.name === newContent.name){
						return `${result.name} exists!`;
					}
					else{
						result.name = newContent.name;
						return result.save().then((updatedTask, err) => {
							if(err){
								console.log(err);
								return false;
							}
							else{
								return updatedTask;
							}
						});
					}
				}
			});
		}

		/*result.name = newContent.name;
		return result.save().then((updatedTask, err) => {
			if(err){
				console.log(err);
				return false;
			}
			else{
				return updatedTask;
			}
		});*/
	});
}

// Controller function for retrieving a specific task.
/*
	Business Logic
	1. Look for the task with the ID provided in the URL.
	2. Display the specific task.
*/
module.exports.getOneTask = (taskId) => {
	return Task.findById(taskId).then((resultTask, err) => {
		if(err){
			console.log(err);
			return false;
		}
		return resultTask;
	});
};

// Controller function for completing tasks
/*
	Business Logic
	1. Find the task thru its ID given in the route.
	2. Change the task's status to "complete".
	3. Save the changes.
*/
module.exports.completeTask = (taskId) => {
	return Task.findById(taskId).then((result, err) => {
		if(err){
			console.log(err);
			return false;
		}
		result.status = "complete";
		return result.save().then((updatedTask, err) => {
			if(err){
				console.log(err);
				return false;
			}
			else{
				return updatedTask;
			}
		});
	});
}